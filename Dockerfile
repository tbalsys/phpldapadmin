FROM alpine:3.13

EXPOSE 80

ENV LDAP_SERVER_URI=127.0.0.1
ENV LDAP_BIND_DN=cn=Manager,dc=example,dc=com

RUN apk add --no-cache phpldapadmin php7-apache2 && \
  sed \
    -e "s#^DocumentRoot \".*#DocumentRoot \"/usr/share/webapps/phpldapadmin\"#g" \
    -e "s#/var/www/localhost/htdocs#/usr/share/webapps/phpldapadmin#" \
    -i /etc/apache2/httpd.conf && \
  printf "\n<Directory \"/usr/share/webapps/phpldapadmin\">\n\tAllowOverride All\n</Directory>\n" >> /etc/apache2/httpd.conf

COPY config.php /usr/share/webapps/phpldapadmin/config/config.php

CMD ["sh", "-c", "sed \
  -e \"s#LDAP_SERVER_URI#$LDAP_SERVER_URI#\" \
  -e \"s#LDAP_BIND_DN#$LDAP_BIND_DN#\" \
  -i /usr/share/webapps/phpldapadmin/config/config.php && \
  exec httpd -D FOREGROUND"]
